package sheridan;

import static org.junit.Assert.*;

import org.junit.Test;

public class EmailValidatorTest {

	@Test
	public void testHasCorrectFormatException() {
		assertFalse("Invalid email address", EmailValidator.isValidEmail("tamana@gmail.com.aa"));
	}

	@Test
	public void testHasCorrectFormatBoundaryIn() {
		assertTrue("Invalid email address", EmailValidator.isValidEmail("tamana@gmail.com"));
	}

	@Test
	public void testHasCorrectFormatBoundaryOut() {
		assertFalse("Invalid email address", EmailValidator.isValidEmail("@gmail.com"));
	}

	@Test
	public void testHasOnlyOneSymbolException() {
		assertFalse("Invalid email address", EmailValidator.isValidEmail("tamana@@gmail.com"));
	}

	@Test
	public void testHasOnlyOneSymbolBoundaryIn() {
		assertTrue("Invalid email address", EmailValidator.isValidEmail("tamana@gmail.com"));
	}

	@Test
	public void testHasOnlyOneSymbolBoundaryOut() {
		assertFalse("Invalid email address", EmailValidator.isValidEmail("tamanagmail.com"));
	}

	@Test
	public void testIsNameLowercaseBoundaryIn() {
		assertTrue("Invalid email address", EmailValidator.isValidEmail("tamana@gmail.com"));
	}

	@Test
	public void testIsNameLowercaseBoundaryOut() {
		assertFalse("Invalid email address", EmailValidator.isValidEmail("Tamana@gmail.com"));
	}

	@Test
	public void testNameStartWithNumberBoundaryIn() {
		assertTrue("Invalid email address", EmailValidator.isValidEmail("tamana123@gmail.com"));
	}

	@Test
	public void testStartWithNumberBoundaryOut() {
		assertFalse("Invalid email address", EmailValidator.isValidEmail("0tamana123@gmail.com"));
	}

	@Test
	public void testHasNameAtleastThreeAlphaCharsRegular() {
		assertTrue("Invalid email address", EmailValidator.isValidEmail("tamama@gmail.com"));
	}

	@Test
	public void testHasNameAtleastThreeAlphaCharsException() {
		assertFalse("Invalid email address", EmailValidator.isValidEmail("ta @gmail.com"));
	}

	@Test
	public void testHasNameAtleastThreeAlphaCharsBoundaryIn() {
		assertTrue("Invalid email address", EmailValidator.isValidEmail("tam@gmail.com"));
	}

	@Test
	public void testHasNameAtleastThreeAlphaCharsBoundaryOut() {
		assertFalse("Invalid email address", EmailValidator.isValidEmail("ta@gmail.com"));
	}

	@Test
	public void testHasDomainAtleastThreeAlphaCharsRegular() {
		assertTrue("Invalid email address", EmailValidator.isValidEmail("tamama@gmail.com"));
	}

	@Test
	public void testHasDomainAtleastThreeAlphaCharsException() {
		assertFalse("Invalid email address", EmailValidator.isValidEmail("tam@gm .com"));
	}

	@Test
	public void testHasDomainAtleastThreeAlphaCharsBoundaryIn() {
		assertTrue("Invalid email address", EmailValidator.isValidEmail("tam@gma.com"));
	}

	@Test
	public void testHasDomainAtleastThreeAlphaCharsBoundaryOut() {
		assertFalse("Invalid email address", EmailValidator.isValidEmail("ta@gm.com"));
	}

	@Test
	public void testHasExtenxionAtleastTwoAlphaCharsRegular() {
		assertTrue("Invalid email address", EmailValidator.isValidEmail("tamana@gmail.com"));
	}

	@Test
	public void testHasExtenxionAtleastTwoAlphaCharsException() {
		assertFalse("Invalid email address", EmailValidator.isValidEmail("ta @gmail.c@m"));
	}

	@Test
	public void testHasExtenxionAtleastTwoAlphaCharsBoundaryIn() {
		assertTrue("Invalid email address", EmailValidator.isValidEmail("tam@gma.ca"));
	}

	@Test
	public void testHasExtenxionAtleastTwoAlphaCharsBoundaryOut() {
		assertFalse("Invalid email address", EmailValidator.isValidEmail("ta@gm.c"));
	}

	@Test
	public void testHasExtenxionNumbersBoundaryIn() {
		assertTrue("Invalid email address", EmailValidator.isValidEmail("tamama@gmail.com"));
	}

	@Test
	public void testHasExtenxionNumbersBoundaryOut() {
		assertFalse("Invalid email address", EmailValidator.isValidEmail("tamana@gmail.c0m"));
	}
}
