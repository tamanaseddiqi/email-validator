package sheridan;

public class EmailValidator {

	public static boolean isValidEmail(String email) {
		return email.matches("^[a-z][a-z0-9]{2,}+@[a-z0-9]{3,}+.[a-zA-Z]{2,}+$");
	}
}
